﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlihtMesseger.Classes
{
    class User
    {
        public string username;
        public string usertoken;
    }

    class Base64
    {
        public static string Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Decode(string plainText)
        {
            var plainTextBytes = Convert.FromBase64String(plainText);
            return Encoding.UTF8.GetString(plainTextBytes);
        }
    }
}
